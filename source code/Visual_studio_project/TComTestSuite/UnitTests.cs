using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Windows.Forms;

namespace SeleniumTests
{
    [TestFixture]
    public class RegisterNewUser
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private Random rnd = new Random();
        private IWebElement ele;
        private IJavaScriptExecutor executor;
        private int uniqueNumber;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "https://www.google.com/";
            verificationErrors = new StringBuilder();

            executor = (IJavaScriptExecutor)driver;
            driver.Manage().Window.Maximize();
            uniqueNumber = rnd.Next(10000);
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheRegisterNewUserTest()
        {
            driver.Navigate().GoToUrl("https://www.hrvatskitelekom.hr/");
            driver.FindElement(By.XPath("//header[@id='app']/ul/li[2]/a")).Click();
            ele = driver.FindElement(By.XPath("/html/body/div[1]/div/div[2]/div/div[4]/a"));
            executor.ExecuteScript("arguments[0].click();", ele);
            string testEmail = "testAccount" + uniqueNumber + (char)64 + "net.hr";
            copyToClipboard();
            driver.FindElement(By.Id("mobileNumber")).SendKeys(testEmail);
            driver.FindElement(By.Id("sendPin")).Click();
            Thread.Sleep(5000);
            driver.FindElement(By.Id("ime")).Click();
            driver.FindElement(By.Id("ime")).SendKeys("testName");
            driver.FindElement(By.Id("prezime")).Click();
            driver.FindElement(By.Id("prezime")).SendKeys("testSurname");
            driver.FindElement(By.Id("contact")).Click();
            driver.FindElement(By.Id("contact")).SendKeys("0910910910");
            driver.FindElement(By.Id("password")).Click();
            driver.FindElement(By.Id("password")).SendKeys("Lozinka012@");
            driver.FindElement(By.Id("repeatedPassword")).Click();
            driver.FindElement(By.Id("repeatedPassword")).SendKeys("Lozinka012@");
            driver.FindElement(By.Id("telekomId")).Clear();
            driver.FindElement(By.Id("telekomId")).Click();
            string testID = "testID" + uniqueNumber;
            copyToClipboard(); 
            driver.FindElement(By.Id("telekomId")).SendKeys(testID);
            driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/div/div/div[6]/div/div[2]/label/div[3]")).Click();
            driver.FindElement(By.Id("btnLogIn")).Click();
            //Thread.Sleep(20000); //Provjera da li se test izvr�ava kako treba
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        private void copyToClipboard() {
            Thread t = new Thread((ThreadStart)(() => {
                Clipboard.SetText("@");
            }));

            // Run your code from a thread that joins the STA Thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            t.Interrupt();
        }
    }

    [TestFixture]
    public class LoginExistingUser
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private IWebElement ele;
        private IJavaScriptExecutor executor;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "https://www.google.com/";
            verificationErrors = new StringBuilder();

            driver.Manage().Window.Maximize();
            executor = (IJavaScriptExecutor)driver;
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheLoginExistingUserTest()
        {
            driver.Navigate().GoToUrl("https://www.hrvatskitelekom.hr/");
            driver.FindElement(By.XPath("//header[@id='app']/ul/li[2]/a")).Click();
            ele = driver.FindElement(By.LinkText("Telekom ID podacima"));
            executor.ExecuteScript("arguments[0].click();", ele);
            driver.FindElement(By.Id("username")).SendKeys("testidxanil");
            ele = driver.FindElement(By.Id("password"));
            executor.ExecuteScript("arguments[0].click();", ele);
            copyToClipboard();
            driver.FindElement(By.Id("password")).SendKeys("testPassword012@");
            driver.FindElement(By.Id("btnLogIn")).Click();
            //Thread.Sleep(20000); //Provjera da li se test izvr�ava kako treba
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        private void copyToClipboard()
        {
            Thread t = new Thread((ThreadStart)(() => {
                Clipboard.SetText("@");
            }));

            // Run your code from a thread that joins the STA Thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            t.Interrupt();
        }
    }

    [TestFixture]
    public class TestNavigationBar
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private IWebElement ele;
        private IJavaScriptExecutor executor;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "https://www.google.com/";
            verificationErrors = new StringBuilder();

            driver.Manage().Window.Maximize();
            executor = (IJavaScriptExecutor)driver;
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheNavigationBarTest()
        {
            driver.Navigate().GoToUrl("https://www.hrvatskitelekom.hr/");
            ele = driver.FindElement(By.XPath("/html/body/form/header/div[2]/section/div[2]/nav/ul/li[1]/ul/li[1]/ul/li[1]/a"));  
            executor.ExecuteScript("arguments[0].click();", ele);
            //Thread.Sleep(20000); //Provjera da li se test izvr�ava kako treba
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }

    [TestFixture]
    public class SearchNavigation
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "https://www.google.com/";
            verificationErrors = new StringBuilder();

            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheSearchNavigationTest()
        {
            driver.Navigate().GoToUrl("https://www.hrvatskitelekom.hr/");
            driver.FindElement(By.XPath("//header[@id='app']/ul/li/a")).Click();
            driver.FindElement(By.Id("q")).Click();
            driver.FindElement(By.Id("q")).Clear();
            driver.FindElement(By.Id("q")).SendKeys("Samsung S10");
            driver.FindElement(By.XPath("//a[@id='search-start']/span")).Click();
            //Thread.Sleep(20000); //Provjera da li se test izvr�ava kako treba
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }

    [TestFixture]
    public class TestVirtualTechnician
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private IJavaScriptExecutor js;
        private IWebElement Element;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "https://www.google.com/";
            verificationErrors = new StringBuilder();

            driver.Manage().Window.Maximize();
            js = (IJavaScriptExecutor)driver;
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheVirtualTechnicianTest()
        {
            driver.Navigate().GoToUrl("https://www.hrvatskitelekom.hr/");
            driver.FindElement(By.CssSelector("#app > section > div > div > div.service-area > a:nth-child(1)")).Click();
            Element = driver.FindElement(By.CssSelector("#aspnetForm > div.wrapper-2020 > section.section-main > div > div.row.justify-content-center > div:nth-child(2) > div > a:nth-child(14)"));
            js.ExecuteScript("arguments[0].scrollIntoView();", Element);
            Thread.Sleep(5000);
            driver.FindElement(By.CssSelector("#aspnetForm > div.wrapper-2020 > section.section-main > div > div.row.justify-content-center > div:nth-child(2) > div > a:nth-child(14)")).Click();
            Element = driver.FindElement(By.CssSelector("#aspnetForm > div.wrapper-2020 > section:nth-child(6) > div > div.row.custom_align > button > a"));;
            js.ExecuteScript("arguments[0].scrollIntoView();", Element);
            driver.FindElement(By.CssSelector("#aspnetForm > div.wrapper-2020 > section:nth-child(6) > div > div.row.custom_align > button > a")).Click();
            //Thread.Sleep(20000); //Provjera da li se test izvr�ava kako treba
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}