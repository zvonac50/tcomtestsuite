# TComTestSuite

MiTTPP Projektni zadatak 2020/2021

Korišteni programi:
Visual Studio (C# .NET Core + NUnit), Katalon Recorder

Potrebni driveri:
GeckoDriver (C:\Windows\chromedriver.exe): https://chromedriver.chromium.org/

Potrebni NuGet paketi:
Microsoft.NET.Test.Sdk: https://www.nuget.org/packages/Microsoft.NET.Test.Sdk/16.5.0?_src=template
MSTest.TestAdapter: https://www.nuget.org/packages/MSTest.TestAdapter/2.1.2?_src=template
MSTest.TestFramework: https://www.nuget.org/packages/MSTest.TestFramework/2.1.2?_src=template
NUnit: https://www.nuget.org/packages/NUnit/3.13.0?_src=template
NUnit3TestAdapter: https://www.nuget.org/packages/NUnit3TestAdapter/3.17.0?_src=template
Selenium.Support: https://www.nuget.org/packages/Selenium.Support/3.141.0?_src=template
Selenium.WebDriver: https://www.nuget.org/packages/Selenium.WebDriver/3.141.0?_src=template
Selenium.WebDriver.GeckoDriver: https://www.nuget.org/packages/Selenium.WebDriver.GeckoDriver/0.29.0?_src=template

Princip rada:
Koraci testova su ručno snimani u programu(Katalon Recorder) kao ekstenzija na Chrome pregledniku.
Dokumentacija je pisana za svaki obrađeni test modula odabrane stranice.
Template koda je generiran u Katalon Recorderu te detaljno optimiziran i modificiran u Visual Studiu.

Testirano je:
Web stranica: https://www.hrvatskitelekom.hr/
Moduli web stranice: Registracija novog korisnika, Prijava postojećeg korisnika, Navigacijska traga početne stranice, Modul "Virtualnog tehničara" (putanja do njega), Tražilica

Source code:
Katalon_code_template -> generirani template testova
Visual_studio_project -> testabilan te optimiziran kod navedenih testova

Dodatno, korištene su:
Selenium.Wait naredbe, Thread.Sleep, Single Thread Control, 
prijava i registarcija korisnika u stvarnom vremenu sa stvarnom bazom podataka testirane web stranice (korišteno u edukativne svrhe, baze podataka web stranice ni u jednom trenutku nisu bile preopterećene)

Autor:
Zvonimir Hajduković, 1. godina diplomskog studija, smjer DRC, FERIT
